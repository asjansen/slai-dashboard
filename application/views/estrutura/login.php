<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Painel de Administração - <?php echo $this->dados_globais['configuracao']->titulo; ?></title>

		<?php
			echo link_tag('./assets/css/bootstrap.min.css');
		?>

		<meta name="author" content="Alexandre Specht Jansen" />
		<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- FAVICON	 -->
		<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.ico'); ?>" />

		<style>
			body {
				background-color: #eaeaea;
			}
		</style>

	</head>

	<body>
		<div class="container">
			<div class="col-md-4 col-centered">
				<br class="clear">
				<br class="clear">
				<h3><?php echo $this->dados_globais['configuracao']->titulo ?></h3>
				<br class="clear">
				<?php if ($this->session->flashdata("alerta")): ?>
					<div class="alert <?php echo $this->session->flashdata("tipo") ?>" role="alert"><?php echo $this->session->flashdata("alerta") ?></div>
				<?php endif ?>
				<form action="<?php echo site_url("login/funcao_login") ?>" method="post">
				  <div class="form-group">
				    <label for="email">E-mail</label>
				    <input type="email" required autofocus class="form-control" id="email" name="email" value="<?php echo $this->session->flashdata("email") ?>">
				  </div>
				  <div class="form-group">
				    <label for="senha">Senha</label>
				    <input type="password" required class="form-control" id="senha" name="senha">
				  </div>
				  <button type="submit" class="btn btn-lg btn-primary">Entrar</button>
				</form>
			</div>
		</div>
	</body>

	<script type="text/javascript" src="<?php echo base_url("./assets/js/jquery.js"); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url("./assets/js/bootstrap.min.js"); ?>"></script>
</html>