				</div>
				<!-- /.container-fluid -->
			</div>
			<!-- /#page-wrapper -->		
		</div>
		<!-- /#wrapper -->
	</body>

	<script type="text/javascript" src="<?php echo base_url("./assets/js/jquery.js"); ?>"></script>
<!-- 	<script type="text/javascript" src="<?php //echo base_url('./assets/js/jquery-ui.min.js'); ?>"></script>	 -->
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo base_url("./assets/js/bootstrap.min.js"); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url("./assets/js/metisMenu.min.js"); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url("./assets/js/sb-admin-2.js"); ?>"></script>

	<!-- jQuery File Upload Dependencies -->
	<script src="<?php echo base_url("assets/js/miniupload/jquery.knob.js") ?> "></script>
	<script src="<?php echo base_url("assets/js/miniupload/jquery.ui.widget.js") ?> "></script>
	<script src="<?php echo base_url("assets/js/miniupload/jquery.iframe-transport.js") ?> "></script>
	<script src="<?php echo base_url("assets/js/miniupload/jquery.fileupload.js") ?> "></script>
	<script src="<?php echo base_url("assets/js/miniupload/scripts.js") ?> "></script>

	<script type="text/javascript" language="javascript" src="<?php echo base_url('./assets/js/fancybox.js'); ?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo base_url('./assets/js/mascaras.js'); ?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo base_url('./assets/js/funcoes.js'); ?>"></script>	
	<script type="text/javascript" language="javascript" src="<?php echo base_url('./assets/js/tinymce.js'); ?>"></script>	

	<script type="text/javascript">

		$(document).ready(function() {

			/* ===== Mascaras ===== */
			$('.mascara-data').mask('00/00/0000');
			$('.mascara-hora').mask('00:00:00');
			$('.mascara-data-hora').mask('00/00/0000 00:00:00');
			$('.mascara-cep').mask('00000-000');
			$('.mascara-numero').mask('00000000');
			$('.mascara-telefone-simples').mask('0000-0000');
			$('.mascara-telefone').mask('(00) 00000-0000');
			$('.mascara-celular').mask('(00) 0 0000.00000');
			$('.mascara-cpf').mask('000.000.000-00', {reverse: true});
			$('.mascara-cnpj').mask('00.000.000/0000-00', {reverse: true});
			$('.mascara-dinheiro').mask('000.000.000.000.000,00', {reverse: true});
			$('.mascara-ip').mask('099.099.099.099');
			$('.mascara-percentual').mask('##0,00%', {reverse: true});

			/* ===== Calendario ===== */
			$( ".datepicker" ).datepicker({ dateFormat: 'dd/mm/yy' });

            $(".toggle-parcelas").hide();

            $("#checkbox-toggle-parcelas").click(function() {
               if($(this).is(':checked')) {
                   $(".toggle-parcelas").show();
               } else {
                   $(".toggle-parcelas").hide();
               }
            });

            let parcela = $(".box-parcelas").html();
            $(".adicionar-parcela").click(function () {
                var de_parcela = $('.ate-parcela:last').val();
                de_parcela = parseInt(de_parcela) + 1;
                $(".box-parcelas").append(parcela);
                if(!isNaN(de_parcela))
                    $('.de-parcela:last').val(de_parcela);
            });

            $('.box-parcelas').on('click', '.remover-parcela', function () {
                var parcela = $(this).closest(".each-parcela");
                parcela.remove();
            });

            $(".gerar-grafico").click(function(event){
                event.preventDefault();
                var favorite = [];
                var base = '<?=base_url()?>';
                $.each($("input[name='dado']:checked"), function(){
                    favorite.push($(this).val());
                });
                if(favorite.length > 0)
                    window.location = base + 'dados/export?output=chart&itens=' + favorite.join(";");
            });

            $(".gerar-excel").click(function(event){
                event.preventDefault();
                var favorite = [];
                var base = '<?=base_url()?>';
                $.each($("input[name='dado']:checked"), function(){
                    favorite.push($(this).val());
                });
                if(favorite.length > 0)
                    window.location = base + 'dados/export?output=excel&itens=' + favorite.join(";");
            });


            $(".botao").attr('disabled','disabled');

            $(".dado").click(function() {
                if( $("input[name='dado']:checked").length > 0 )
                    $(".botao").removeAttr('disabled');
                else
                    $(".botao").attr('disabled','disabled');
            });
		});

        $(function() {


        });

	</script>
</html>
