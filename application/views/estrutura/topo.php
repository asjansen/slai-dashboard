<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Painel de Administração - <?php echo $this->dados_globais['configuracao']->titulo; ?></title>

	<?php
				echo link_tag('./assets/css/bootstrap.min.css');
				echo link_tag('./assets/css/fancybox.css');
				echo link_tag('./assets/css/jquery-ui.css');
				echo link_tag('./assets/css/metisMenu.min.css');
				echo link_tag('./assets/css/timeline.css');
				echo link_tag('./assets/css/sb-admin-2.css');
				//echo link_tag('./assets/css/font-awesome.min.css');
				echo link_tag('./assets/css/custom.css');
				echo link_tag('./assets/css/style_uploader.css');
		?>
	  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<!--     <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
 		<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.ico'); ?>" />

</head>

<body>
	<input type="hidden" id="base" value="<?php echo base_url(); ?>">
	<input type="hidden" id="controller" value="<?php echo $this->router->class ?>">
	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Abrir/fechar menu</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo base_url() ?>">Painel de controle - <?php echo $this->dados_globais['configuracao']->titulo; ?></a>
			</div>
			<!-- /.navbar-header -->

			<ul class="nav navbar-top-links navbar-right">
				<!-- /.dropdown -->
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="<?php echo site_url("administradores/editar/".$this->session->userdata('usuario_id')) ?>"><i class="fa fa-user fa-fw"></i> <?php echo $this->session->userdata('usuario_nome') ?></a></li>
						<?php if($this->session->userdata('usuario_adm') == 1) { ?>
                            <li><a href="<?php echo site_url("administradores") ?>"><i class="fa fa-users fa-fw"></i> Usuários</a></li>
                        <?php } ?>
						<li><a href="<?php echo site_url("configuracoes") ?>"><i class="fa fa-cog fa-fw"></i> Configurações</a></li>
						<li class="divider"></li>
						<li><a href="<?php echo site_url("login/funcao_logout") ?>"><i class="fa fa-sign-out fa-fw"></i> Sair</a></li>
					</ul>
					<!-- /.dropdown-user -->
				</li>
				<!-- /.dropdown -->
			</ul>
			<!-- /.navbar-top-links -->

			<div class="navbar-default sidebar" role="navigation">
				<div class="sidebar-nav navbar-collapse">
					<ul class="nav" id="side-menu">
						

						<li><a href="<?php echo site_url('dados/enviar') ?>"><i class="fa fa-upload fa-fw"></i> Enviar dados</a></li>
						
						<li><a href="<?php echo site_url('dados') ?>"><i class="fa fa-list fa-fw"></i> Listar dados</a></li>
						
						<!-- <li class="<?php //if($this->router->class == 'dados' ) echo 'active' ?>">
							<a href="#"> Dados <span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li><a href="<?php //echo site_url('dados/cadastrar') ?>">Enviar</a></li>
								<li><a href="<?php //echo site_url('dados/listar') ?>">Listar</a></li>
							</ul>
						</li> -->

					</ul>
				</div>
				<!-- /.sidebar-collapse -->
			</div>
			<!-- /.navbar-static-side -->
		</nav>
		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<?php if($this->session->flashdata('alerta')) { ?>
					<br class="clear">
					<div class="alert <?php echo $this->session->flashdata('tipo') ?> alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<?php echo $this->session->flashdata('alerta') ?>
					</div>
				<?php } ?>
