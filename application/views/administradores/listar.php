<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo ucfirst($this->router->class) ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				Lista
			</div>            
			<div class="panel-body">
				<div class="col-lg-12">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th style="width:120px;">Ações</th>
									<th>Nome</th>
									<th>E-mail</th>
									<th>Data</th>
									<th>Status</th>
									<th>Imagem</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($objetos as $objeto) { ?>
									<tr>
										<td class="text-center">
											<a href="<?php echo site_url($this->router->class.'/editar/'.$objeto->id) ?>" title="Editar" class="btn btn-info btn-circle" ><i class="fa fa-edit"></i></a>
											<?php if($objeto->liberado == 1) { ?>
												<a <?php if( $this->session->userdata('usuario_id') == $objeto->id) echo "disabled" ?> href="<?php echo site_url($this->router->class.'/alterar_status/'.$objeto->id) ?>" title="Bloquear" class="btn btn-warning btn-circle" ><i class="fa fa-lock"></i></a>
											<?php } else { ?>
												<a <?php if( $this->session->userdata('usuario_id') == $objeto->id) echo "disabled" ?> href="<?php echo site_url($this->router->class.'/alterar_status/'.$objeto->id) ?>" title="Liberar" class="btn btn-success btn-circle" ><i class="fa fa-unlock"></i></a>
											<?php } ?>
											<a  <?php if( $this->session->userdata('usuario_id') == $objeto->id) echo "disabled" ?> href="<?php echo site_url($this->router->class.'/excluir/'.$objeto->id) ?>" onclick="return confirmar()" title="Excluir" class="btn btn-danger btn-circle" ><i class="fa fa-times"></i></a>
										</td>
										<td><?php echo $objeto->nome ?></td>
										<td><?php echo $objeto->email ?></td>
										<td><?php echo converter_data($objeto->data) ?></td>
										<td><?php echo $objeto->liberado == 1 ? "<span class=\"label label-success\">Liberado</span>" : "<span class=\"label label-danger\">Bloqueado</span>" ?></td>
										<td class="text-center">
											<?php
												if( file_exists('./assets/upload/'.$this->router->class.'/'.$objeto->imagem) && $objeto->imagem ) {
													$imagem = base_url('assets/upload/'.$this->router->class.'/'.$objeto->imagem);
													echo "<a href=\"$imagem\" class=\"fancybox btn btn-primary btn-circle\" title=\"$objeto->nome\" rel=\"carrosel\"><i class=\"fa fa-camera\"></i></a>";
												}
											?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
						<div class="pull-right">
							<a href="<?php echo site_url($this->router->class.'/cadastrar') ?>" class="btn btn-success">Cadastrar</a>
							<?php echo $this->pagination->create_links() ?>
						</div>
					</div>
					<!-- /.table-responsive -->
				</div>
			</div>
		</div>
	</div>
</div>