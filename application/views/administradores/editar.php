<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo ucfirst($this->router->class) ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
	<form role="form" action="<?php echo site_url($this->router->class.'/funcao_editar/'.$objeto->id) ?>" onsubmit="return confirma_senha()"  method="post" enctype="multipart/form-data">
		<div class="col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Editar
				</div>            
				<div class="panel-body">
					<div class="col-lg-12">
                        <div class="row">
                            <div class="form-group">
                                <label for="nome">Nome</label>
                                <input type="text" class="form-control" id="nome" name="nome" value="<?php echo $objeto->nome ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input type="email" class="form-control" id="email" name="email" value="<?php echo $objeto->email ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="liberado">Status</label>
                                <select name="liberado" id="liberado" class="form-control" required>
									<option <?php if($objeto->liberado == 1) echo 'selected' ?> value="1">Liberado</option>
									<option <?php if($objeto->liberado == 2) echo 'selected' ?> value="2">Bloqueado</option>
								</select>
                            </div>
							<div class="col-lg-3">
								<div class="form-group">
									<label for="senha">Senha</label>
									<input type="password" class="form-control" id="senha" name="senha">
								</div>
							</div>
							<div class="col-lg-3">
								<div class="form-group">
									<label for="confirmacao">Confirmação</label>
									<input type="password" class="form-control" id="confirmacao" name="confirmacao">
								</div>
							</div>
							<br class="clear">
							<div class="form-group">
								<label for="imagem">Imagem</label>
								<p class="help-block"> Tamanho máximo <?php echo $this->dados_globais['configuracao']->maximo_upload ?>Kb.</p>
								<input type="file" name="imagem" id="imagem">
							</div>
						</div>
						<div class="pull-right">
							<button type="submit" class="btn btn-success">Salvar</button>
							<a href="<?php echo site_url($this->router->class.'/listar') ?>" class="btn btn-primary">Voltar</a>
							<?php if($objeto->id != $this->session->userdata('usuario_id')) { ?>
								<a href="<?php echo site_url($this->router->class.'/excluir/'.$objeto->id) ?>" onclick="return confirmar()" class="btn btn-danger">Excluir</a>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<?php if( file_exists('./assets/upload/'.$this->router->class.'/'.$objeto->imagem) && $objeto->imagem ) { ?>
				<div class="col-lg-4 col-md-offset-4">
					<div class="panel panel-primary">
						<div class="panel-heading">
							Imagem atual
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12">
									<a href="<?php echo base_url("assets/upload/".$this->router->class.'/'.$objeto->imagem) ?>" class="fancybox">
										<img src="<?php echo base_url("assets/upload/".$this->router->class.'/'.$objeto->imagem) ?>" class="img-thumbnail">
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</form>
</div>