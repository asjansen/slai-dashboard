<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Configurações</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
	<form role="form" action="<?php echo site_url("configuracoes/funcao_editar") ?>" method="post">
		<div class="col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Informações gerais
				</div>            
                <div class="panel-body">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="form-group">
                                <label for="titulo">Título do site</label>
                                <input type="text" class="form-control" id="titulo" name="titulo" value="<?php echo $this->dados_globais['configuracao']->titulo ?>" required>
                            </div>
                            <?php /*
							<div class="row">
								<div class="col-lg-4">
									<div class="form-group">
										<label for="maximo_upload">Tamanho máximo de upload</label>
										<p class="help-block"> Tamanho máximo de arquivos para upload. Em Kb.</p>
										<input type="text" class="form-control mascara-numero" id="maximo_upload" name="maximo_upload" value="<?php echo $this->dados_globais['configuracao']->maximo_upload ?>" required>
									</div>
								</div>
							</div>
                            <div class="form-group">
                                <label for="email">E-mail principal</label>
                                <p class="help-block">E-mail de destino dos formulários do site. </p>
                                <input type="email" class="form-control" id="email" name="email" value="<?php echo $this->dados_globais['configuracao']->email ?>" required>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="email_smtp">E-mail SMTP</label>
                                    <p class="help-block">E-mail utilizado para envio de formulários. Deve ser um e-mail do servidor local. </p>
                                    <input type="email" class="form-control" id="email_smtp" name="email_smtp" value="<?php echo $this->dados_globais['configuracao']->email_smtp ?>" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="senha_smtp">Senha SMTP</label>
                                    <p class="help-block">Senha do e-mail utilizado para validação de envio de formulários. </p>
                                    <input type="password" class="form-control" id="senha_smtp" name="senha_smtp" value="<?php echo $this->dados_globais['configuracao']->senha_smtp ?>" required>
                                </div>
                            </div>
                            */ ?>
                        </div>
                    </div>
                </div>
			</div>
		</div>
		<?php /*
		<div class="col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Informações de contato
				</div>            
                <div class="panel-body">
                    <div class="col-lg-12">
                        <div class="row">
							<div class="col-lg-4">
								<div class="form-group">
									<label for="rua">Rua</label>
									<input type="text" class="form-control" id="rua" name="rua" value="<?php echo $this->dados_globais['configuracao']->rua ?>">
								</div>
							</div>
							<div class="col-lg-2">
								<div class="form-group">
									<label for="numero">Número</label>
									<input type="text" class="form-control" id="numero" name="numero" value="<?php echo $this->dados_globais['configuracao']->numero ?>">
								</div>
							</div>
							<div class="col-lg-2">
								<div class="form-group">
									<label for="complemento">Complemento</label>
									<input type="text" class="form-control" id="complemento" name="complemento" value="<?php echo $this->dados_globais['configuracao']->complemento ?>">
								</div>
							</div>
							<div class="col-lg-2">
								<div class="form-group">
									<label for="bairro">Bairro</label>
									<input type="text" class="form-control" id="bairro" name="bairro" value="<?php echo $this->dados_globais['configuracao']->bairro ?>">
								</div>
							</div>
							<div class="col-lg-2">
								<div class="form-group">
									<label for="cep">CEP</label>
									<input type="text" class="form-control mascara-cep" id="cep" name="cep" value="<?php echo $this->dados_globais['configuracao']->cep ?>">
								</div>
							</div>
                        </div>
                        <div class="row">
							<div class="col-lg-3">
								<div class="form-group">
									<label for="cidade">Cidade/UF</label>
									<input type="text" class="form-control" id="cidade" name="cidade" value="<?php echo $this->dados_globais['configuracao']->cidade ?>">
								</div>
							</div>
							<div class="col-lg-2">
								<div class="form-group">
									<label for="telefone1">Telefone Aline</label>
									<input type="text" class="form-control mascara-celular" id="telefone1" name="telefone1" value="<?php echo $this->dados_globais['configuracao']->telefone1 ?>" required>
								</div>
							</div>
							<div class="col-lg-2">
								<div class="form-group">
									<label for="telefone2">Telefone Sidnei</label>
									<input type="text" class="form-control mascara-celular" id="telefone2" name="telefone2" value="<?php echo $this->dados_globais['configuracao']->telefone2 ?>">
								</div>
							</div>
							<div class="col-lg-2">
								<div class="form-group">
									<label for="telefone3">Telefone 3</label>
									<input type="text" class="form-control mascara-celular" id="telefone3" name="telefone3" value="<?php echo $this->dados_globais['configuracao']->telefone3 ?>">
								</div>
							</div>
							<div class="col-lg-3">
								<div class="form-group">
									<label for="google_maps">Coordenadas Google Maps</label>
									<input type="text" class="form-control" id="google_maps" name="google_maps" value="<?php echo $this->dados_globais['configuracao']->google_maps ?>">
								</div>
							</div>
						</div>
                    </div>
                </div>
			</div>
		</div>
		
		<div class="col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Redes sociais
				</div>            
                <div class="panel-body">
                    <div class="col-lg-12">
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label for="facebook">Facebook</label>
									<input type="url" class="form-control" id="facebook" name="facebook" value="<?php echo $this->dados_globais['configuracao']->facebook ?>">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label for="twitter">Pinterest</label>
									<input type="url" class="form-control" id="twitter" name="twitter" value="<?php echo $this->dados_globais['configuracao']->twitter ?>">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label for="instagram">Instagram</label>
									<input type="url" class="form-control" id="instagram" name="instagram" value="<?php echo $this->dados_globais['configuracao']->instagram ?>">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label for="google">Google +</label>
									<input type="url" class="form-control" id="google" name="google" value="<?php echo $this->dados_globais['configuracao']->google ?>">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		*/ ?>
		<div class="col-lg-12">
			<button type="submit" class="btn btn-success pull-right">Salvar alterações</button>
			<br class="clear">
			<br class="clear">
		</div>
	</form>
</div>

