<style>
    /* Customize the label (the container) */
    .container-cb {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: 22px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default checkbox */
    .container-cb input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    /* Create a custom checkbox */
    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 25px;
        width: 25px;
        background-color: #ccc;
    }

    /* On mouse-over, add a grey background color */
    .container-cb:hover input ~ .checkmark {
        background-color: #ccc;
    }

    /* When the checkbox is checked, add a blue background */
    .container-cb input:checked ~ .checkmark {
        background-color: #2196F3;
    }

    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the checkmark when checked */
    .container-cb input:checked ~ .checkmark:after {
        display: block;
    }

    /* Style the checkmark/indicator */
    .container-cb .checkmark:after {
        left: 9px;
        top: 5px;
        width: 5px;
        height: 10px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }
</style>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo ucfirst($this->router->class) ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Filtros
            </div>
            <div class="panel-body">
                <form>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="data-de">Enviado de</label>
                            <input type="text" class="form-control datepicker" value="<?=$this->input->get("data-de") ?>" name="data-de" id="data-de" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="data-ate">Enviado até</label>
                            <input type="text" class="form-control datepicker" value="<?=$this->input->get("data-ate") ?>" name="data-ate" id="data-ate" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="tratamento">Tratamento</label>
                            <input type="text" class="form-control" value="<?=$this->input->get("tratamento") ?>" name="tratamento" id="tratamento" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="criador">Criado por</label>
                            <select name="criador" id="criador" class="form-control">
                                <option value=""></option>
                                <?php foreach($administradores as $administrador) { ?>
                                    <option <?=$administrador->id == $this->input->get("criador") ? 'selected' : '' ?> value="<?=$administrador->id?>"><?=$administrador->nome?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <input type="submit" class="btn btn-success" value="Buscar">
                        <a href="<?=site_url('dados')?>" class="btn btn-info">Limpar busca</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				Lista
			</div>            
			<div class="panel-body">
				<div class="col-lg-12">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
                                    <th>Selecionar</th>
                                    <th>Id Envio</th>
                                    <th>Data envio</th>
                                    <th>Tratamento</th>
                                    <th>Proporção</th>
                                    <th>Criado por</th>
                                    <th style="width:80px;">Remover</th>
                                </tr>
							</thead>
							<tbody>
								<?php foreach($objetos as $objeto) { ?>
									<tr>
                                        <td>
                                            <label class="container-cb">
                                                <input type="checkbox" value="<?=$objeto->envio_id.'-'.$objeto->tratamento?>" class="dado" name="dado">
                                                <span class="checkmark"></span>
                                            </label>
                                        </td>
                                        <td><?php echo $objeto->envio_id ?></td>
                                        <td><?php echo converter_data($objeto->data_envio) ?></td>
                                        <td><?php echo $objeto->tratamento ?></td>
                                        <td><?php echo $objeto->proporcao ?></td>
                                        <td><?php echo $objeto->administrador_nome ?></td>
                                        <td class="text-center">
                                            <a href="<?php echo site_url($this->router->class.'/excluir?envio_id='.$objeto->envio_id.'&tratamento='.$objeto->tratamento) ?>" onclick="return confirmar()" title="Excluir" class="btn btn-danger btn-circle" ><i class="fa fa-times"></i></a>
                                        </td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
						<div class="pull-right">
							<a href="#" class="btn btn-info gerar-grafico botao"><i class="fa fa-chart-bar"></i> Gerar gráfico</a>
							<a href="#" class="btn btn-info gerar-excel botao"><i class="fa fa-download"></i> Exportar xlsx</a>
						</div>
                        <?php echo $this->pagination->create_links() ?>
                    </div>
					<!-- /.table-responsive -->
				</div>
			</div>
		</div>
	</div>
</div>
