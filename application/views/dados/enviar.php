<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo ucfirst($this->router->class) ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
	<form role="form" action="<?php echo site_url($this->router->class.'/enviar') ?>" method="post" enctype="multipart/form-data">
		<div class="col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Enviar
				</div>            
				<div class="panel-body">
					<div class="col-lg-12">
                        <div class="row">
							<div class="form-group">
								<label for="arquivo-movel">Sensor móvel</label>
								<input type="file" name="arquivo-movel" id="arquivo-movel">
                                <p class="help-block"> Arquivo .txt</p>
                            </div>
                            <div class="form-group">
								<label for="arquivo-fixo">Sensor fixo</label>
								<input type="file" name="arquivo-fixo" id="arquivo-fixo">
                                <p class="help-block"> Arquivo .txt</p>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="checkbox-toggle-parcelas" name="checkbox-nomes[]" value="sim">
                                    Informar nomes
                                </label>
                            </div>
                            <div class="box-parcelas toggle-parcelas">
                                <div class="each-parcela">
                                    <div class="col-lg-7">
                                        <div class="form-group">
                                            <label>Nome parcela</label>
                                            <input type="text" class="form-control" name="nome-parcela[]">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label>De</label>
                                            <input type="text" class="form-control mascara-numero de-parcela" name="de-parcela[]">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label>Até</label>
                                            <input type="text" class="form-control mascara-numero ate-parcela" name="ate-parcela[]">
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <i style="padding-top:25px;cursor:pointer;" title="Remover parcela" class="fa fa-2x fa-times-circle remover-parcela"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 toggle-parcelas">
                                <div class="pull-left">
                                    <i style="padding-top:25px;cursor:pointer;" title="Adicionar parcela" class="fa fa-2x fa-plus-circle adicionar-parcela"></i>
                                </div>
                            </div>
                        </div>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-success">Enviar</button>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
