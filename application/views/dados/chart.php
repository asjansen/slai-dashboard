<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawBasic);

    function drawBasic() {

        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Trat');
        data.addColumn('number', 'Lux');

        data.addRows([
            //['P1',3.5154878],
            <?php
                foreach ($retorno as $retorno) {
                    echo "['".$retorno['tratamento']."',".$retorno['proporcao']."],";
                }
            ?>
        ]);

        var options = {
            title: 'Título do Gráfico',
            hAxis: {
                title: 'Tratamento',
                // viewWindow: {
                //     min: [7, 30, 0],
                //     max: [17, 30, 0]
                // }
            },
            vAxis: {
                title: 'Lux'
            }
        };

        var chart = new google.visualization.ColumnChart(
            document.getElementById('chart_div'));


        chart.draw(data, options);
    }

</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Gráfico</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Visualizar
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div id="chart_div"></div>
                    </div>
                </div>
                <div class="pull-right">
                    <a href="<?php echo site_url($this->router->class) ?>" class="btn btn-info">Listar</a>
                </div>
            </div>
        </div>
    </div>
</div>
