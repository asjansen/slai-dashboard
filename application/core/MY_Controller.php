<?php
    class MY_Controller extends CI_Controller {

        public $dados_globais;

        function __construct() {

            parent::__construct();

            //VERIFICA SE ESTÁ LOGADO
            if( !$this->session->userdata('logado') && $this->router->class != 'login' )
                redirect('login');
            //FIM VERIFICACAO
            
            $c = new Configuracao();
            $this->dados_globais['configuracao'] = $c->get();
            
            function buscar_mes($mes){

                switch( $mes ){

                    case '01' : return 'janeiro'; break;
                    case '02' : return 'fevereiro'; break;
                    case '03' : return 'março'; break;
                    case '04' : return 'abril'; break;
                    case '05' : return 'maio'; break;
                    case '06' : return 'junho'; break;
                    case '07' : return 'julho'; break;
                    case '08' : return 'agosto'; break;
                    case '09' : return 'setembro'; break;
                    case '10' : return 'outubro'; break;
                    case '11' : return 'novembro'; break;
                    case '12' : return 'dezembro'; break;
                }
            }
            /* ===== FUNÇÃO BUSCAR MÊS ===== */
            function formata_valor($entrada) {

                $saida = str_replace('.', '', $entrada);
                $saida = str_replace(',', '.', $saida);
                return $saida;
            }

            // pega valor com ponto no decimal e transforma para o valor BR
            function mostrar_valor($entrada) {

                if($entrada)
                    return number_format( $entrada, 2, ',', '.' );
                else
                    return false;
            }

            /* ===== FUNÇÃO LIMITAR TEXTO ===== */ 
            function limita_texto($texto, $tamanho){
                //Diminui o texto apenas se ele for MAIOR que o tamanho informado
                $texto = strip_tags($texto);
                if( strlen($texto) >= $tamanho )
                    return substr($texto, 0, strrpos(substr($texto, 0, $tamanho), ' ')) . '...';
                else
                    return $texto;
            }
            /* ===== FIM FUNÇÃO LIMITAR TEXTO ===== */            

            /* ===== FLASHDATA ===== */
            function flashdata($mensagem, $tipo){

                switch( $tipo ){
                    case 'sucesso': $t = 'alert-success'; break;
                    case 'erro': $t = 'alert-danger'; break;
                    case 'neutro': $t = 'alert-info'; break;
                    case 'aviso': $t = 'alert-warning'; break;
                    default: $t = 'alert-info'; break;
                }

                $CI = get_instance();

                $CI->session->set_flashdata('alerta', $mensagem);
                $CI->session->set_flashdata('tipo', $t);

            }
            /* ===== FIM FLASHDATA ===== */

            /* ===== FUNÇÃO PADRÃO DE UPLOAD ===== */
            function upload( $pasta,$fonte,$nome ){
                /* PASTA = A partir de assets/upload */
                /* FONTE = Nome do campo do arquivo */
                /* NOME = Nome para salvar o arquivo */
                $c = new Configuracao();
                $c->select('maximo_upload')->get(1);

                $config['upload_path']   = "./assets/upload/$pasta";
                $config['allowed_types'] = '*';
                $config['file_name']     = $nome;
                $config['max_size']      = $c->maximo_upload;
                /*$config['max_width']   = '1024';
                $config['max_height']    = '768';*/
                $config['overwrite']     = true;

                $CI = get_instance();
                $CI->load->library('upload', $config);
                $CI->upload->initialize($config);

                return $CI->upload->do_upload($fonte);

            }
            /* ===== FIM FUNÇÃO PADRÃO DE UPLOAD ===== */

            function thumb($pasta,$arquivo,$largura,$altura) {

                /* PASTA = Para para ler a imagem atual e salvar a imagem thumb */
                /* ARQUIVO = Nome do arquivo original */
                /* LARGURA = Largura para redimensionar a imagem, em pixels, apenas números */
                /* ALTURA = Altura para redimensionar a imagem, em pixels, apenas números */

                $config['image_library']  = 'gd2';
                $config['source_image']   = "./assets/upload/$pasta/$arquivo";
                $config['create_thumb']   = TRUE;
                $config['maintain_ratio'] = TRUE;
                $config['width']          = $largura;
                $config['height']         = $altura;
                $config['new_image']      = "./assets/upload/$pasta";

                $CI = get_instance();
                $CI->load->library('image_lib', $config); 
                $CI->image_lib->resize();            

            }
            
            function marca_dagua($caminho) {
                
                $config['source_image']	= './assets/upload/'.$caminho;
                $config['wm_type'] = 'overlay';
                $config['wm_vrt_alignment'] = 'bottom';
                $config['wm_hor_alignment'] = 'right';
                $config['wm_hor_offset'] = '20';
                $config['wm_vrt_offset'] = '20';
                //$config['wm_padding'] = '20';
                $config['wm_overlay_path'] = './assets/img/marcadagua.png';
                $config['wm_opacity'] = '40';

                $CI = get_instance();
                $CI->load->library('image_lib');
                $CI->image_lib->initialize($config);
                $CI->image_lib->watermark();
            }

            /* ===== FUNÇÃO PADRÃO DE ENVIO DE E-MAIL ===== */
            function enviar_email($assunto ,$mensagem, $destinatario){

                $c = new Configuracao();
                $data['configuracoes'] = $c->get();

                require_once('./assets/php/phpmailer/class.phpmailer.php');

                $mail = new PHPMailer();
                $mail->IsSMTP(); 
                $mail->Host     = 'localhost';
                $mail->CharSet = 'UTF-8';
                $mail->SMTPAuth = true; 
                $mail->Username = $data['configuracoes']->email_smtp; //email cadastrado no servidor
                $mail->Password = $data['configuracoes']->senha_smtp; // senha cadastrada no servidor
                $mail->From     = $data['configuracoes']->email_smtp; // Remetente Email
                $mail->FromName = $data['configuracoes']->titulo; // titulo da mensagem
                $mail->AddAddress($destinatario, ''); //Destinatário
                $mail->WordWrap = 50; 
                $mail->IsHTML(true); //enviar em HTML
                $mail->AddReplyTo($data['configuracoes']->email_smtp, '');  //e-mail e nome de resposta
                $mail->Subject = $assunto; //ASSUNTO
                //adicionando o html no corpo do email
                $mail->Body = $mensagem;

                return $mail->Send() ? true : false;

            }
            /* ===== FIM FUNÇÃO PADRÃO DE ENVIO DE E-MAIL ===== */


            /* ===== FUNÇÃO AMIGAVEL ===== */
            function amigavel($str){

                $friendlyURL = htmlentities($str, ENT_COMPAT, "UTF-8", false); 
                $friendlyURL = preg_replace('/&([a-z]{1,2})(?:acute|lig|grave|ring|tilde|uml|cedil|caron);/i','\1',$friendlyURL);
                $friendlyURL = html_entity_decode($friendlyURL,ENT_COMPAT, "UTF-8"); 
                $friendlyURL = preg_replace('/[^a-z0-9-]+/i', '-', $friendlyURL);
                $friendlyURL = preg_replace('/-+/', '-', $friendlyURL);
                $friendlyURL = trim($friendlyURL, '-');
                $friendlyURL = strtolower($friendlyURL);
                return $friendlyURL;

            }
            /* ===== FIM FUNÇÃO AMIGAVEL ===== */


            /* ===== FUNÇÃO BUSCAR THUMB ===== */
            /* Recebe o nome da imagem e retorna o nome da imagem thumb. EX: 2139812.jpg, retorna 2139812_thumb.jpg */
            function buscar_thumb($imagem){
                $thumb = explode('.', $imagem);
                return "$thumb[0]_thumb.$thumb[1]";
            }
            /* ===== FIM FUNÇÃO BUSCAR THUMB ===== */



             /* ===== FUNÇÃO EXTENSÃO ===== */
            function extensao($arquivo){

                return strtolower( pathinfo($arquivo, PATHINFO_EXTENSION) );

            }
            /* ===== FIM FUNÇÃO EXTENSÃO ===== */

            /* ===== FUNÇÃO CONVERTER DATA ===== */
            function converter_data($entrada){
                /* Converte para o formato OPOSTO. Exemplo: 28/08/1990 para 1990-08-28 e vice-versa. */

                if($entrada) {
                    if( strpos($entrada, '-') === false ){//Converte para o formato 1990-08-28
                        $saida = explode('/', $entrada);
                        return "$saida[2]-$saida[1]-$saida[0]";
                    }
                    else {//Converte para o formato 28/08/1990
                        if( strpos($entrada, ':') === false ){//Se não tiver hora converte normalmente
                            $saida = explode('-', $entrada);
                            return "$saida[2]/$saida[1]/$saida[0]";
                        }
                        else{//Se tiver a hora separa ela da data e converte os dois
                            $saida = explode(' ', $entrada);
                            $data  = explode('-', $saida[0]);
                            $hora  = explode(':', $saida[1]);
                            return "$data[2]/$data[1]/$data[0] $hora[0]:$hora[1]";
                        }                
                    }
                }
                else {
                    return false;
                }
            }
            /* ===== FIM FUNÇÃO CONVERTER DATA ===== */
            
    function valorPorExtenso( $valor = 0, $bolExibirMoeda = true, $bolPalavraFeminina = false ) {

        //$valor = formata_valor( $valor );
                        //$saida = str_replace('.', '', $valor);
//                $valor = str_replace(',', '.', $saida);
           

        $singular = null;
        $plural = null;

        if ( $bolExibirMoeda )
        {
            $singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
            $plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões","quatrilhões");
        }
        else
        {
            $singular = array("", "", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
            $plural = array("", "", "mil", "milhões", "bilhões", "trilhões","quatrilhões");
        }

        $c = array("", "cem", "duzentos", "trezentos", "quatrocentos","quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
        $d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta","sessenta", "setenta", "oitenta", "noventa");
        $d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze","dezesseis", "dezesete", "dezoito", "dezenove");
        $u = array("", "um", "dois", "três", "quatro", "cinco", "seis","sete", "oito", "nove");


        if ( $bolPalavraFeminina )
        {
        
            if ($valor == 1) 
            {
                $u = array("", "uma", "duas", "três", "quatro", "cinco", "seis","sete", "oito", "nove");
            }
            else 
            {
                $u = array("", "um", "duas", "três", "quatro", "cinco", "seis","sete", "oito", "nove");
            }
            
            
            $c = array("", "cem", "duzentas", "trezentas", "quatrocentas","quinhentas", "seiscentas", "setecentas", "oitocentas", "novecentas");
            
            
        }


        $z = 0;

        $valor = number_format( $valor, 2, ".", "." );
        $inteiro = explode( ".", $valor );

        for ( $i = 0; $i < count( $inteiro ); $i++ ) 
        {
            for ( $ii = mb_strlen( $inteiro[$i] ); $ii < 3; $ii++ ) 
            {
                $inteiro[$i] = "0" . $inteiro[$i];
            }
        }

        // $fim identifica onde que deve se dar junção de centenas por "e" ou por "," ;)
        $rt = null;
        $fim = count( $inteiro ) - ($inteiro[count( $inteiro ) - 1] > 0 ? 1 : 2);
        for ( $i = 0; $i < count( $inteiro ); $i++ )
        {
            $valor = $inteiro[$i];
            $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
            $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
            $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

            $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd && $ru) ? " e " : "") . $ru;
            $t = count( $inteiro ) - 1 - $i;
            $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
            if ( $valor == "000")
                $z++;
            elseif ( $z > 0 )
                $z--;
                
            if ( ($t == 1) && ($z > 0) && ($inteiro[0] > 0) )
                $r .= ( ($z > 1) ? " de " : "") . $plural[$t];
                
            if ( $r )
                $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
        }

        $rt = mb_substr( $rt, 1 );

        return($rt ? trim( $rt ) : "zero");

    }

            /* ==================== FIM FUNÇÕES GLOBAIS ==================== */
        }
    }