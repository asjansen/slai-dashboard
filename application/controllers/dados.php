<?php
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Dados extends MY_Controller {

    public function index() {

        $itens = 10; //Itens por página
        $page = $this->input->get('page') ? $this->input->get('page') : 1;//Página

        $d = new Dado();
        $d->select('envio_id, data_envio, tratamento, avg(proporcao) as proporcao, (select nome from administrador where id = administrador_id limit 1) as administrador_nome');
        $d->group_by('envio_id, tratamento');
        $d->order_by('data_envio', 'desc');

        if($this->input->get('data-de'))
            $d->where('data_envio >=', converter_data($this->input->get('data-de')).' 00:00:00');
        if($this->input->get('data-ate'))
            $d->where('data_envio <=', converter_data($this->input->get('data-ate')).' 23:59:59');
        if($this->input->get('tratamento'))
            $d->ilike('tratamento',$this->input->get('tratamento'),'both');
        if($this->input->get('criador'))
            $d->where("administrador_id",$this->input->get('criador'));

        $data['objetos'] = $d->get_paged($page,$itens);

        //contagem
        $d = new Dado();

        if($this->input->get('data-de'))
            $d->where('data_envio >=', converter_data($this->input->get('data-de')).' 00:00:00');
        if($this->input->get('data-ate'))
            $d->where('data_envio <=', converter_data($this->input->get('data-ate')).' 23:59:59');
        if($this->input->get('tratamento'))
            $d->ilike('tratamento',$this->input->get('tratamento'),'both');
        if($this->input->get('criador'))
            $d->where("administrador_id",$this->input->get('criador'));

        $d->group_by('envio_id, tratamento')->get();

        $total = $d->result_count();

        $params = $_SERVER['QUERY_STRING'];
        $url = $params ? site_url($this->router->class.'?'.$params) : site_url($this->router->class.'?g=1');
        $url = preg_replace('/&?page=[^&]*/', '', $url);

        $this->load->library('pagination');
        $config['base_url']             = $url;
        $config['total_rows']           = $total;
        $config['per_page']             = $itens;
        $config['use_page_numbers']     = TRUE;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open']        = "<nav><ul class='pagination'>";
        $config['full_tag_close']       = "</ul></nav>";
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a href='#'>";
        $config['cur_tag_close']        = "</a></li>";
        $config['next_tag_open']        = "<li>";
        $config['next_tagl_close']      = "</li>";
        $config['prev_tag_open']        = "<li>";
        $config['prev_tagl_close']      = "</li>";
        $config['first_tag_open']       = "<li>";
        $config['first_tagl_close']     = "</li>";
        $config['last_tag_open']        = "<li>";
        $config['last_tagl_close']      = "</li>";
        $config['first_link']           = 'Primeira';
        $config['last_link']            = 'Última';
        $config['next_link']            = 'Próxima';
        $config['prev_link']            = 'Anterior';

        $this->pagination->initialize($config);

        $d = new Administrador();
        $data['administradores'] = $d->get();

        $this->load->view('estrutura/topo');
        $this->load->view('dados/listar',$data);
        $this->load->view('estrutura/rodape');

    }

    public function enviar()    {

        ini_set("auto_detect_line_endings", true);

        if($this->input->post()) {
            $extmovel   = strtolower( pathinfo( $_FILES['arquivo-movel']['name'], PATHINFO_EXTENSION ) );
            $extfixo    = strtolower( pathinfo( $_FILES['arquivo-fixo']['name'], PATHINFO_EXTENSION ) );

            $arquivomovel   = $_FILES['arquivo-movel']['name'];
            $arquivofixo    = $_FILES['arquivo-fixo']['name'];

            $pasta = date("Ymd-His");
            mkdir('./assets/upload/'.$this->router->class.'/'.$pasta,0777);

            if( $extmovel == 'txt' && $extfixo == 'txt'){
                upload($this->router->class.'/'.$pasta,'arquivo-movel',$arquivomovel);
                upload($this->router->class.'/'.$pasta,'arquivo-fixo',$arquivofixo);

                $dadosMovel = array();
                $dadosFixo = array();

                /* ======================================== LEITURA DO ARQUIVO MOVEIS ======================================== */

                $arquivo = fopen (base_url("assets/upload/dados/$pasta/$arquivomovel"), 'r');
                $ii=0;

                while(!feof($arquivo)) {

                    $linha = fgets($arquivo);

                    $linha = explode(';',$linha);

                    if(count($linha) == 6) { // sem coluna de nome

                        $data = explode('/',$linha[1]);
                        $lux = str_replace('.','',str_replace(',','',$linha[4]));
                        $hora = explode('*', $linha[5]);
                    } else { // com coluna de nome
                        $data = explode('/',$linha[2]);
                        $lux = str_replace('.','',str_replace(',','',$linha[5]));
                        $hora = explode('*', $linha[6]);
                    }

                    $hora = trim($hora[0]);

                    $datahora = date('Y').'-'.$data[1].'-'.$data[0].' '.$hora;

                    if(isset($_POST['checkbox-nomes'][0]) && $_POST['checkbox-nomes'][0] == 'sim') {
                        $tratamento = 'N/I';
                        for($i=0;$i<count($this->input->post('nome-parcela'));$i++) {
                            if($ii >= ($_POST['de-parcela'][$i] - 1) && $ii <= ($_POST['ate-parcela'][$i] - 1 ))
                                $tratamento = $_POST['nome-parcela'][$i];
                        }
                    } else {
                        if(count($linha) == 6)
                            $tratamento = 'N/I';
                        else
                            $tratamento = $linha[0];
                    }

                    $dado = array('data' => $datahora, 'lux' => $lux, 'tratamento' => $tratamento);
                    array_push($dadosMovel,$dado);
                    $ii++;
                }
                fclose($arquivo);

                /* ======================================== LEITURA DO ARQUIVO FIXOS ======================================== */

                $arquivo = fopen (base_url("assets/upload/dados/$pasta/$arquivofixo"), 'r');
                while(!feof($arquivo)) {
                    $linha = fgets($arquivo, 1024);
                    $linha = explode(';',$linha);

                    $data = explode('/',$linha[1]);
                    $lux = str_replace('.','',str_replace(',','',$linha[4]));
                    $hora = explode('*', $linha[5]);
                    $hora = trim($hora[0]);

                    $datahora = date('Y').'-'.$data[1].'-'.$data[0].' '.$hora;

                    $dado = array('data' => $datahora, 'lux' => $lux);
                    array_push($dadosFixo,$dado);
                }
                fclose($arquivo);

                /* ======================================== FILTRAGEM DOS DADOS ======================================== */

                $dadosFiltrados = array();
                foreach($dadosFixo as $dadoFixo) {

                    $keyMovel = array_search($dadoFixo['data'], array_column($dadosMovel, 'data'));
                    if($keyMovel !== false) {
                        $coeficiente = $dadosMovel[$keyMovel]['lux'] / ($dadoFixo['lux'] - $dadosMovel[$keyMovel]['lux']);
                        $proporcao = $dadosMovel[$keyMovel]['lux'] / $dadoFixo['lux'];

                        $dadoFiltrado = array(
                                            'key' => $keyMovel,
                                            'data' => $dadosMovel[$keyMovel]['data'],
                                            'tratamento' => $dadosMovel[$keyMovel]['tratamento'],
                                            'coeficiente' => number_format($coeficiente,'6','.',''),
                                            'proporcao' => $proporcao
                                            );
                        if($proporcao <= 1)
                            array_push($dadosFiltrados,$dadoFiltrado);
                    }
                }

//                echo "<pre>";
//                print_r($dadosFiltrados);
//                echo "</pre>";

                /* ======================================== INSERÇÃO DOS DADOS NO DB ======================================== */
                $envio_id = microtime(true); // id para agrupar os resultados por ele e tratamento
                foreach($dadosFiltrados as $dado) {
                    $d = new Dado();
                    $d->envio_id = $envio_id;
                    $d->administrador_id = $this->session->userdata("usuario_id");
                    $d->data_envio = date('Y-m-d H:i:s');
                    $d->data = $dado['data'];
                    $d->tratamento = $dado['tratamento'];
                    $d->coeficiente = $dado['coeficiente'];
                    $d->proporcao = $dado['proporcao'];
                    $d->save();
                }
                flashdata("Upload realizado","sucesso");
                redirect($this->router->class);

            }
            else {
                flashdata("Arquivo(s) devem estar em txt","erro");
                redirect($this->router->class.'/enviar');
            }

        } else {

            $this->load->view('estrutura/topo');
            $this->load->view('dados/enviar');
            $this->load->view('estrutura/rodape');
        }
    }

    public function export() {

        if($this->input->get('itens') == '') {
            flashdata("Nenhum dado selecionado","erro");
            redirect($this->router->class);
        }

        $itens = explode(';',$this->input->get('itens'));

        $d = new Dado();
        $d->select('envio_id, data_envio, tratamento, avg(proporcao) as proporcao');
        for($i=0;$i<count($itens);$i++) {
            $item = explode('-', $itens[$i]);
            $envio_id = $item[0];
            $tratamento = $item[1];

            $d->or_group_start();
            $d->where("envio_id", $envio_id);
            $d->where("tratamento", $tratamento);
            $d->group_end();
        }
        $d->group_by('envio_id, tratamento');
        $objetos = $d->get();

        /* faz os calculos para a geração do gráfico */
        $retornoArray = array();
        foreach($objetos as $objeto) {

            $proporcao = $objeto->proporcao;
            $proporcao_log = log($objeto->proporcao);
            $linear = $this->calcLinear($proporcao_log);
            $exp = $this->calcExp($proporcao);

            /*Resultados do modelo linear menores que 7.15 devem ser substituidos pelos resultados do modelo exponencial.*/
            $value = $linear;
            if($linear < 7.15)
                $value = $exp;

            $retorno = array('envio_id' => $objeto->envio_id, 'data_envio' => $objeto->data_envio, 'tratamento' => $objeto->tratamento, 'proporcao' => $value);

            array_push($retornoArray,$retorno);
        }
        $data['retorno'] = $retornoArray;

        if($this->input->get('output') == 'chart') {

            $this->load->view('estrutura/topo');
            $this->load->view('dados/chart', $data);
            $this->load->view('estrutura/rodape');
        }
        elseif($this->input->get('output') == 'excel') {
            $this->excel($retornoArray);
        }
        else
            redirect($this->router->class);
    }

    public function calcLinear($value = null) {

        if($value == null)
            return false;

        $intercept = -0.7085168;
        $coef = -2.3745846;
        return $intercept + ($coef * $value);
    }

    public function calcExp($value = null) {

        if($value == null)
            return false;

        $intercept = 2.168967;
        $coef = -5.502273;
        return exp($intercept + ($coef * $value));
    }

    public function excel($objetos) {

        $filename = 'export-slai-'.microtime(true);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Envio ID');
        $sheet->setCellValue('B1', 'Data envio');
        $sheet->setCellValue('C1', 'Tratamento');
        $sheet->setCellValue('D1', 'Proporção');

        $i=1;
//        echo "<pre>";
//        print_r($objetos);
//        echo "</pre>";
//        exit;
        foreach($objetos as $objeto) {
            $i++;
            $sheet->setCellValue('A'.$i, $objeto['envio_id']);
            $sheet->setCellValue('B'.$i, converter_data($objeto['data_envio']));
            $sheet->setCellValue('C'.$i, $objeto['tratamento']);
            $sheet->setCellValue('D'.$i, $objeto['proporcao']);
        }

        $writer = new Xlsx($spreadsheet);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); /*-- $filename is  xsl filename ---*/
        header('Cache-Control: max-age=0');
        $writer->save("php://output");

    }

    public function excluir() {

        $d = new Dado();
        $d->where("tratamento", $this->input->get('tratamento'));
        $d->where("envio_id", $this->input->get('envio_id'));
        $d->get();
        $d->delete_all();

        flashdata("Dados excluídos","sucesso");
        redirect($this->router->class);
    }

}
