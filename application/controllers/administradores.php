<?php

	class Administradores extends MY_Controller {
		
		public function index() {

            if($this->session->userdata('usuario_adm') == 1)
			    redirect($this->router->class.'/listar');
            else
                redirect($this->router->class.'/editar/'.$this->session->userdata('usuario_id'));
		}
		
		public function listar() {

            if($this->session->userdata('usuario_adm') != 1)
                redirect($this->router->class.'/editar/'.$this->session->userdata('usuario_id'));
			
			$itens = 10;//Itens por página
			$page = $this->input->get('page') ? $this->input->get('page') : 1;//Página
			
			$o = new Administrador();
			$o->order_by("data","desc");
			$data['objetos'] = $o->get_paged($page,$itens);
			
			//contagem
			$o = new Administrador();
			$total = $o->count();
			
			/* concatena os parametros setados na url */
			$url = site_url($this->router->class.'/listar?g=1');
			/*retira o parametro 'page' da url, senão fica repetindo a cada clique em uma pagina */
			//$url = preg_replace('/&?page=[^&]*/', '', $url);
			
			$this->load->library('pagination');
			$config['base_url'] 			= $url;
			$config['total_rows'] 			= $total;
			$config['per_page'] 			= $itens; 
			$config['use_page_numbers'] 	= TRUE;
			$config['page_query_string'] 	= TRUE;
			$config['query_string_segment'] = 'page';
			$config['full_tag_open'] 		= "<nav><ul class='pagination'>";
			$config['full_tag_close'] 		= "</ul></nav>";
			$config['num_tag_open'] 		= '<li>';
			$config['num_tag_close'] 		= '</li>';
			$config['cur_tag_open'] 		= "<li class='active'><a href='#'>";
			$config['cur_tag_close'] 		= "</a></li>";
			$config['next_tag_open'] 		= "<li>";
			$config['next_tagl_close'] 		= "</li>";
			$config['prev_tag_open'] 		= "<li>";
			$config['prev_tagl_close'] 		= "</li>";
			$config['first_tag_open'] 		= "<li>";
			$config['first_tagl_close'] 	= "</li>";
			$config['last_tag_open'] 		= "<li>";
			$config['last_tagl_close'] 		= "</li>";
			$config['first_link'] 			= 'Primeira';
			$config['last_link'] 			= 'Última';
			$config['next_link'] 			= 'Próxima';
			$config['prev_link'] 			= 'Anterior';

			$this->pagination->initialize($config);
			
			$this->load->view('estrutura/topo');
			$this->load->view($this->router->class.'/listar',$data);
			$this->load->view('estrutura/rodape');
		}
		
		public function cadastrar() {

            if($this->session->userdata('usuario_adm') != 1)
                redirect($this->router->class.'/editar/'.$this->session->userdata('usuario_id'));

			$this->load->view('estrutura/topo');
			$this->load->view($this->router->class.'/cadastrar');
			$this->load->view('estrutura/rodape');
		}
		
		public function editar() {
			
			$id = $this->uri->segment(3);

            if($this->session->userdata('usuario_adm') != 1 && $this->session->userdata('usuario_id') != $id)
                redirect($this->router->class.'/editar/'.$this->session->userdata('usuario_id'));
			
			$o = new Administrador();
			$data['objeto'] = $o->where('id',$id)->get();
			
			if(!$o->result_count()) {
				flashdata("Nenhum dado encontrado.","erro");
				redirect($this->router->class.'/listar');
			}
			
			$this->load->view('estrutura/topo');
			$this->load->view($this->router->class.'/editar',$data);
			$this->load->view('estrutura/rodape');			
		}
		
		public function funcao_cadastrar() {
			
			if($this->input->post()) {
				
				$o = new Administrador();
				
				$o->nome 		= strip_tags($this->input->post("nome"));
				$o->email 		= strip_tags($this->input->post("email"));
				$o->senha 		= sha1(strip_tags($this->input->post("senha")));
				$o->liberado 	= strip_tags($this->input->post("liberado"));
				$o->data 		= date("Y-m-d H:i:s");

				if( $o->save() ) {
					
					$id = $o->id;
				
					if( !empty($_FILES['imagem']['name']) ){
						$ext = strtolower( pathinfo( $_FILES['imagem']['name'], PATHINFO_EXTENSION ) );
						$per = array('jpg','jpeg','png');
						$imagem = rand(0, 9999999999).'.'.$ext;

						if( in_array($ext, $per) ){
							if( upload($this->router->class,'imagem',$imagem) ){
								
								$o->where("id",$id)->update('imagem',$imagem);
								thumb($this->router->class,$o->imagem,120,120);
								flashdata("Cadastrado com sucesso.","sucesso");
								redirect($this->router->class.'/listar/');
							} 
							else {
								flashdata("Dados cadastrados, porém houve um erro ao enviar a imagem. Verifique tamanho limite.","aviso");
								redirect($this->router->class.'/editar/'.$id);
							}
						}
						else {
							flashdata("Dados cadastrados, porém houve um erro ao enviar a imagem. Extensão não permitida.","aviso");
							redirect($this->router->class.'/editar/'.$id);
						}
					}
					else {
						flashdata("Cadastrado com sucesso.","sucesso");
						redirect($this->router->class.'/listar');
					}
				}
				else {
					flashdata("Ocorreu um erro ao salvar.","erro");
					redirect($this->router->class.'/cadastrar/');
				}
			}
			else
				redirect($this->router->class);
		}
		
		public function funcao_editar() {

			$id = $this->uri->segment(3);
			
			$o = new Administrador();
			$o->where("id",$id)->get(1);
			
			if($o->imagem) {
				$antiga = $o->imagem;
				$antiga_thumb = buscar_thumb($o->imagem);
			}
			
			if($o->result_count()) {
				
				$update = array(
								'nome' => strip_tags($this->input->post("nome")),
								'email' => strip_tags($this->input->post("email")),
								'liberado' => strip_tags($this->input->post("liberado"))
								);
				
				if($this->input->post('senha'))
					$o->where("id",$id)->update('senha',sha1(strip_tags($this->input->post("senha"))));

				$o->where("id",$id)->update($update);
				
				$ext = strtolower( pathinfo( $_FILES['imagem']['name'], PATHINFO_EXTENSION ) );
				$per = array('jpg','jpeg','png');

				if( !empty($_FILES['imagem']['name']) ){
					
					if(in_array($ext,$per)) {
						
						$nova   = rand(0, 9999999999).'.'.$ext;

						if( upload($this->router->class,'imagem',$nova) ){
						
							thumb($this->router->class,$nova,120,120);
							if($antiga)
								unlink("./assets/upload/".$this->router->class."/".$antiga);
							if($antiga_thumb)
								unlink('./assets/upload/'.$this->router->class.'/'.$antiga_thumb);

							$o->where('id',$id)->update('imagem', $nova);
							
							flashdata("Dados alterados.","sucesso");
							redirect($this->router->class.'/listar');
						}
						else {
							flashdata("Dados alterados, porém houve um erro ao enviar a imagem. Verifique tamanho limite.","aviso");
							redirect($this->router->class.'/editar/'.$id);
						}
					}
					else {
						flashdata("Dados alterados, porém houve um erro ao enviar a imagem. Extensão não permitida.","aviso");
						redirect($this->router->class.'/editar/'.$id);
					}
				}
				else {
					flashdata("Dados alterados.","sucesso");
					redirect($this->router->class.'/listar');
				}
			}
			else
				flashdata("Nenhum dado encontrado.","erro");
			
			redirect($this->router->class.'/listar');
		}
		
		public function alterar_status() {
			
			$id = $this->uri->segment(3);

            if($this->session->userdata('usuario_adm') != 1 && $this->session->userdata('usuario_id') != $id)
                redirect($this->router->class.'/editar/'.$this->session->userdata('usuario_id'));
			
			$o = new Administrador();
			$o->where("id",$id)->get(1);
			
			if($o->result_count()) {
				if($o->liberado == 1) {
					$status = 2;
					$mensagem = "Usuário bloqueado.";
				}
				else {
					$status = 1;
					$mensagem = "Usuário liberado.";
				}
				
				if($o->where("id",$id)->update("liberado",$status))
					flashdata($mensagem,"sucesso");
				else
					flashdata("Ocorreu um erro.","erro");
			}
			else
				flashdata("Nenhum dado encontrado.","erro");
			
			redirect($this->router->class.'/listar');
		}
		
		public function excluir() {
			
			$id = $this->uri->segment(3);

            if($this->session->userdata('usuario_adm') != 1)
                redirect($this->router->class.'/editar/'.$this->session->userdata('usuario_id'));
			
			$o = new Administrador();
			$o->where("id",$id)->get(1);
			$banner = $o->imagem;
	
			if($o->result_count()) {
				if($o->delete()) {
					if($banner)
						unlink('./assets/upload/'.$this->router->class.'/'.$banner);
					//unlink('./assets/upload/'.$this->router->class.'/'.buscar_thumb($banner));
					flashdata("Excluído com sucesso.","sucesso");
				}
				else
					flashdata("Ocorreu um erro ao excluir.","erro");
			}
			else
				flashdata("Nenhum dado encontrado.","erro");
			
			redirect($this->router->class.'/listar');
		}
	}
