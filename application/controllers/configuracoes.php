<?php
    class Configuracoes extends MY_Controller {
        
        public function index() {
            
			$this->load->view('estrutura/topo');
			$this->load->view('configuracoes/index');
			$this->load->view('estrutura/rodape');
        }
		
		public function funcao_editar() {
			
			if($this->input->post()) {
				
				$update = array(
								'titulo' => strip_tags($this->input->post('titulo'))
//								'maximo_upload' => strip_tags($this->input->post('maximo_upload')),
//								'email' => strip_tags($this->input->post('email')),
//								'email_smtp' => strip_tags($this->input->post('email_smtp')),
//								'senha_smtp' => strip_tags($this->input->post('senha_smtp'))
// 								'rua' => strip_tags($this->input->post('rua')),
// 								'numero' => strip_tags($this->input->post('numero')),
// 								'complemento' => strip_tags($this->input->post('complemento')),
// 								'bairro' => strip_tags($this->input->post('bairro')),
// 								'cep' => strip_tags($this->input->post('cep')),
// 								'cidade' => strip_tags($this->input->post('cidade')),
// 								'telefone1' => strip_tags($this->input->post('telefone1')),
// 								'telefone2' => strip_tags($this->input->post('telefone2')),
// 								'telefone3' => strip_tags($this->input->post('telefone3')),
// 								'google_maps' => strip_tags($this->input->post('google_maps')),
//								'facebook' => prep_url(strip_tags($this->input->post('facebook'))),
//								'twitter' => prep_url(strip_tags($this->input->post('twitter'))),
//								'instagram' => prep_url(strip_tags($this->input->post('instagram'))),
// 								'google' => prep_url(strip_tags($this->input->post('google')))
								);
				
				$c  = new Configuracao();
				
				if($c->where("id",1)->update($update))
					flashdata("Dados alterados","sucesso");
				else
					flashdata("Nenhum dado alterado","aviso");
				
				redirect("configuracoes");
			}
			else
				redirect("configuracoes");
		}
		
		public function sql() {
			
			
 			//$query = $this->db->query("explain pasta");

			//$this->db->query("alter table pasta drop publicado");
			//$this->db->query("alter table cliente add column ultimo_acesso datetime default '0000-00-00 00:00:00'");
			//$this->db->query("delete from acomodacao_foto");
			//$this->db->query("ALTER TABLE pasta modify column parent_id int(11) default 0");
			//$this->db->query("ALTER TABLE administrador CHANGE foto imagem varchar(255);");
			
// 			$this->db->query("DROP TABLE convenio");
			
//     			$this->db->query("
//     							CREATE TABLE empresa (
//     								id INT(11) AUTO_INCREMENT PRIMARY KEY,
//     								imagem varchar(255) default null,
//   									descricao text default null
//     							);
//     							");
			
// 				$this->db->query("insert into empresa(id,imagem,descricao) value(1,'','descricao')");
//   			$this->db->query("
//   							CREATE TABLE projeto (
//   								id INT(11) AUTO_INCREMENT PRIMARY KEY,
//  								nome varchar(255) not null,
// 								tipo varchar(255) default null,
// 								cliente varchar(255) default null,
//   								descricao text default null,
//  								data datetime default current_timestamp,
//  								publicado int(1) default 1
//   							);
//   							");
			
// 			$this->db->query("insert into convenio(descricao) value('lalal')");
// 			$this->db->query("insert into servico(descricao) value('lalal')");
//   			$this->db->query("
//   						CREATE TABLE projeto_foto (
//   							id INT(11) AUTO_INCREMENT PRIMARY KEY,
//   							projeto_id int(11) NOT NULL,
//   							imagem VARCHAR(255) NOT NULL,
//   							capa int(1) default 0,
//   							publicado int(1) default 1,
//  							data datetime default CURRENT_TIMESTAMP
//   						);
//   						");
		}
	}
