<?php 

	class Login extends MY_Controller {

		public function index() {

			if($this->session->userdata('logado'))
				redirect('home');

			$this->load->view('estrutura/login');

		}

		public function funcao_login() {

			if($this->session->userdata('logado'))
				redirect('home');			

			if($this->input->post()) {

				$a = new Administrador();
				$condicao = array(
								'email' => strip_tags($this->input->post("email")),
								'senha' => sha1(strip_tags($this->input->post("senha"))),
								'liberado' => 1
								);

				$a->where($condicao)->get();

				if($a->result_count()) {
					$this->session->set_userdata("logado", true);
					$this->session->set_userdata("usuario_nome", $a->nome);
					$this->session->set_userdata("usuario_id", $a->id);
					$this->session->set_userdata("usuario_adm", $a->administrador);
					redirect("home");
				}
				else {
					flashdata("Dados não encontrados","erro");
					$this->session->set_flashdata('email', $this->input->post("email"));
					redirect('login');
				}

			}
			else
				redirect();
		}

		public function funcao_logout() {
			
			$nome = $this->session->userdata("usuario_nome");

			$this->session->unset_userdata("logado");
			$this->session->unset_userdata("usuario_nome");
			$this->session->unset_userdata("usuario_id");
			$this->session->unset_userdata("usuario_adm");

			flashdata("Até breve, $nome","sucesso");			
			redirect('login');
		}
}
