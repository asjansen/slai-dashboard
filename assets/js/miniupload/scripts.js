$(function(){

    var ul = $('#upload ul');

    $('#drop a').click(function(){
        // Simulate a click on the file input button
        // to show the file browser dialog
        $(this).parent().find('input').click();
    });

    // Initialize the jQuery File Upload plugin
    $('#upload').fileupload({

        // This element will accept file drag/drop uploading
        dropZone: $('#drop'),

        // This function is called when a file is added to the queue;
        // either via the browse button, or via drag/drop:
        add: function (e, data) {

            var tpl = $('<li class="working"><input type="text" value="0" data-width="48" data-height="48"'+
                ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p></p><span></span></li>');

            // Append the file name and file size
            tpl.find('p').text(data.files[0].name)
                         .append('<i>' + formatFileSize(data.files[0].size) + '</i>');

            // Add the HTML to the UL element
            data.context = tpl.appendTo(ul);

            // Initialize the knob plugin
            tpl.find('input').knob();

            // Listen for clicks on the cancel icon
            tpl.find('span').click(function(){

                if(tpl.hasClass('working')){
                    jqXHR.abort();
                }

                tpl.fadeOut(function(){
                    tpl.remove();
                });

            });

            var jqXHR = data.submit().success(function(result, textStatus, jqXHR){
                
                var pasta = $("#pasta").val();
                atualiza_lista(pasta);

                var json = JSON.parse(result);
                var status = json['status'];
                

                if(status == 'error'){
                    data.context.addClass('error');
                }

                setTimeout(function(){
                    data.context.fadeOut('slow');
                },3000);
            });
        },

        progress: function(e, data){

            // Calculate the completion percentage of the upload
            var progress = parseInt(data.loaded / data.total * 100, 10);

            // Update the hidden input field and trigger a change
            // so that the jQuery knob plugin knows to update the dial
            data.context.find('input').val(progress).change();

            if(progress == 100){
                data.context.removeClass('working');
            }
        },

        fail:function(e, data){
            // Something has gone wrong!
            data.context.addClass('error');
        }

    });


    // Prevent the default action when a file is dropped on the window
    $(document).on('drop dragover', function (e) {
        e.preventDefault();
    });

    // Helper function that formats the file sizes
    function formatFileSize(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }

        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }

        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }

        return (bytes / 1000).toFixed(2) + ' KB';
    }

});

function atualiza_lista(pasta) {

    var id  = $("#id").val();
    var base  = $("#base").val();
    var contr  = $("#controller").val();
    if(!pasta)
        pasta = '';

    $.ajax({
        url: base + contr + '/lista_arquivos?id=' + id + '&pasta=' + pasta,
        context: document.body
    }).done(function(retorno) {
        $("#lista").html(retorno);
        $("#pasta").val(pasta);
        /* ordenação de imagens */
        $('#sortable_photos').sortable({
            update: function (event, ui) {
              var data = $(this).sortable('serialize');
              var base = $("#base").val();
              var controller = $("#controller").val();
              $(".fa-check").addClass('hidden');
              $(".fa-spinner").removeClass('hidden');
              console.log(data);
              console.log(base);
              console.log(controller);
              // POST to server using $.post or $.ajax
              $.ajax({
                  data: data,
                  type: 'POST',
                  url: base + controller + '/ordenar_fotos'
              }).done(function(retorno) {
                console.log(retorno);
                $(".fa-spinner").addClass('hidden');
                $(".fa-check").removeClass('hidden');
              });          
          }
        });       
        $("#sortable_photos").disableSelection();
        $('#myModal').on('hidden.bs.modal', function (e) {
          atualiza_lista();
        })      
    });
}

function capa_arquivo(arquivo_id) {
    
    var base  = $("#base").val();
    var contr  = $("#controller").val();

    $.ajax({
        url: base + contr + '/capa_arquivo/' + arquivo_id,
        context: document.body
    }).done(function(retorno) {
        var pasta = $("#pasta").val();
        atualiza_lista(pasta);
    });    
}

function status_arquivo(arquivo_id) {
    
    var base  = $("#base").val();
    var contr  = $("#controller").val();

    $.ajax({
        url: base + contr + '/alterar_status_arquivo/' + arquivo_id,
        context: document.body
    }).done(function(retorno) {
        var pasta = $("#pasta").val();
        atualiza_lista(pasta);
    });
}

function excluir_arquivo(arquivo_id) {
    
    if(confirm('Você tem certeza?')) {
        
        var base  = $("#base").val();
        var contr  = $("#controller").val();

        $.ajax({
            url: base + contr + '/excluir_arquivo/' + arquivo_id,
            context: document.body
        }).done(function(retorno) {
            var pasta = $("#pasta").val();
            atualiza_lista(pasta);
        });
    }
    else 
        return false;    
}

function cadastrar_link() {
	
	var $btn = $("#botao-cadastrar-link");
	$btn.button('loading');
	
	var base = $("#base").val();
	var id = $("#id").val();
	var nome = $("#nome_link").val();
	var url = $("#url_link").val();
	var parent_id = $("#pasta").val();
	
	if(!nome) {
		alert("Preencha o nome");
		$("#nome_link").focus();
		$btn.button('reset');
		return false;
  }
 	if(!url) {
		alert("Preencha o link");
		$("#url_link").focus();
		$btn.button('reset');
		return false;
  }
 
	$.ajax({
// 		url: base + 'clientes/funcao_cadastrar_link?id=' + id + '&nome=' + nome + '&url=' + url + '&parent_id=' + parent_id,
    url: base + 'clientes/funcao_cadastrar_link',
    type: "POST",
    data: {
      'id': id, 
      'nome': nome,
      'url': url,
      'parent_id': parent_id
    },
		context: document.body
	}).done(function(retorno) {
//     console.log(base + 'clientes/funcao_cadastrar_link?id=' + id + '&nome=' + nome + '&url=' + url + '&parent_id=' + parent_id);
//     console.log(retorno);
		
		$btn.button('reset');
		if(retorno == 'erro') {
			$("#label-nome").addClass('has-error');
			alert("Ocorreu um erro ao salvar");
		}
		else if (retorno == 'existe') {
			alert("Link já existente");
			$("#label-url").addClass('has-warning');
			$("#url_link").val('');
			$("#url_link").focus();
		}
		else {
			atualiza_lista(parent_id);
			$('#modal_link').modal('hide');
			$("#nome_link").val('');
			$("#url_link").val('');
			$("#pasta").val(retorno);
		}
	});
}



function cadastrar_pasta() {
	
	var $btn = $("#botao-cadastrar-pasta");
	$btn.button('loading');
	
	var base = $("#base").val();
	var id = $("#id").val();
	var nome = $("#nome_pasta").val();
	var parent_id = $("#pasta").val();
	
	if(!nome) {
		alert("Preencha o nome");
		$("#nome_pasta").focus();
		$btn.button('reset');
		return false;
	}		
	
	$.ajax({
		url: base + 'clientes/funcao_cadastrar_pasta?id=' + id + '&nome=' + nome + '&parent_id=' + parent_id,
		context: document.body
	}).done(function(retorno) {
		
		$btn.button('reset');
		if(retorno == 'erro') {
			$("#label-nome").addClass('has-error');
			alert("Ocorreu um erro ao salvar");
		}
		else if (retorno == 'existe') {
			alert("Pasta já existente");
			$("#label-nome").addClass('has-warning');
			$("#nome_pasta").val('');
			$("#nome_pasta").focus();
		}
		else {
			atualiza_lista(parent_id);
			$('#modal_pasta').modal('hide');
			$("#nome_pasta").val('');
			$("#pasta").val(retorno);
		}
	});
}

function buscar_pastas(id) {
	
	var base = $("#base").val();
	
	$.ajax({
		url: base + 'clientes/buscar_pastas?id=' + id,
		context: document.body
	}).done(function(retorno) {
		$("#pasta, #parent_id").html(retorno);
	});
}

function excluir_pasta(id) {
	
    if(confirm('Isso irá excluir todo o seu conteúdo, você tem certeza?')) {
        var base = $("#base").val();

        $.ajax({
            url: base + 'clientes/excluir_pasta/' + id,
            context: document.body
        }).done(function(retorno) {
            var pasta = $("#pasta").val();
            atualiza_lista(pasta);
        });
    }
    else
        return false;
}
