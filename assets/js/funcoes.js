function ajax_novo(event, div, url, efeito){

	if(event.preventDefault){event.preventDefault();}else{event.returnValue=false;};

	if( efeito ){
		$(div).animate({ opacity: 0 }, function(){
			$(div).load(url, function(){
				$(div).animate({ opacity: 1 });
			});
		});
	}
	else {
		$(div).load(url);
	}

}

/* ==================== FIM AJAX ==================== */

/* ==================== FUNÇÕES DA GALERIA ==================== */

function excluir_foto(event, i, url, imagem, id){

	//i      = numero do elemento para ser escondido
	//url    = url da funcao que exclui a imagem
	//imagem = NOME da imagem para ser excluida
	//id 	 = Id da galeria

	if(event.preventDefault){event.preventDefault();}else{event.returnValue=false;};
	
	//Não exclui, caso o item atual seja a CAPA
	if( $('.item-'+i).children('.icone-capa').is(":visible") ){
		alert("Você precisa selecionar outra foto como principal antes de excluir esta foto!");
	}
	else {
		$('.item-'+i).fadeOut(300);

		$.ajax({ type: "GET",   
	        url: url+'?imagem='+imagem+'&galeria='+id,   
	        async: false
		});


	}

}


function selecionar_capa(event, i, id, url, imagem){

	//i      = numero do elemento para ser escondido
	//id 	 = id da galeria
	//url    = url da funcao que exclui a imagem
	//imagem = NOME da imagem para ser excluida

	if(event.preventDefault){event.preventDefault();}else{event.returnValue=false;};

	//Cahama a função apenas se o item ainda não é a capa
	if( !$('.item-'+i).children('.icone-capa').is(":visible") ){
		
		$('.item-galeria').children('.icone-capa').fadeOut(300);
		$('.item-'+i).children('.icone-capa').fadeIn(300);

		$.ajax({ type: "GET",   
	        url: url+'?imagem='+imagem+'&galeria='+id,   
	        async: false,
		});

	}
}
function confirmar(mensagem){
	if(!mensagem)
		mensagem  = 'Você tem certeza?';
	
	return confirm(mensagem);
}

function confirma_senha() {
	
	var senha = $("#senha").val();
	var confirmacao = $("#confirmacao").val();
	
	
	if($("#enviar_email").is(":checked") && senha === '') {
		alert("Para enviar o e-mail você precisa preencher o campo senha, não necessariamente com a senha cadastrada anteriormente");
		return false;
	}
	
	if(senha == confirmacao) 
		return true;
	else {
		alert("As senhas não conferem");
		$("#confirmacao").val('').focus();
		return false;
	}
}

function verifica_usuario(usuario,id) {
	
	var base = $("#base").val();
	
	$.ajax({
		url: base + 'clientes/verificar_usuario?id=' + id + '&usuario=' + usuario,
		context: document.body
	}).done(function(retorno) {
		
		if(retorno == 0)
			return true;
		else {
			alert("Nome de usuário existente!");
			$("#usuario").val('');
			$("#usuario").focus();
		}
	});	
}

/* =========================== parcelas do cliente ============================= */
function lista_parcelas() {
	
	var cliente_id = $("#cliente_id").val();
	var base = $("#base").val();
	
	$.ajax({
		url: base + 'clientes/lista_parcelas?cliente_id=' + cliente_id,
		context: document.body
	}).done(function(retorno) {
		$("#table-parcelas").html(retorno);
	});
	
}

function cadastrar_parcela() {
	
	var $btn = $('#btn-parcela').button('loading')
	var base = $("#base").val();
	var cliente_id = $("#cliente_id").val();
	var valor = $("#valor").val();
	var vencimento = $("#vencimento").val();

	$("#alerta-parcela").addClass('hidden');
	
	$.ajax({
		url: base + 'clientes/cadastrar_parcela',
		type: "GET",
		data: {
			cliente_id: cliente_id,
			valor: valor,
			vencimento: vencimento
		},
		context: document.body
	}).done(function(retorno) {
		var ret = JSON.parse(retorno);
		if(ret.success) {
			$("#vencimento").val(ret.prox_vencimento);
			$("#alerta-parcela").removeClass('hidden').removeClass('alert-danger').addClass("alert-success").html(ret.mensagem);
			lista_parcelas();
		}
		else
			$("#alerta-parcela").removeClass('hidden').removeClass('alert-success').addClass("alert-danger").html(ret.mensagem);

		$btn.button('reset');
	});
}

function modal_parcela(parcela_id) {
	
	var base = $("#base").val();

	$.ajax({
		url: base + 'clientes/modal_parcela?parcela_id=' + parcela_id,
		context: document.body
	}).done(function(retorno) {
		var r = JSON.parse(retorno);
		if(r.success) {
			$("#ModalParcela #parcela_id").val(parcela_id);
			$("#ModalParcela #valor").val(r.valor);
			$("#ModalParcela #vencimento").val(r.vencimento);
			$("#ModalParcela #status").val(r.status);
			$("#ModalParcela #data_pagamento").val(r.data_pagamento);
			habilita_pagamento(r.status);
			$('#ModalParcela').modal('toggle');
		}
		else
			alert("Nenhuma parcela encontrada");
	});
}

function habilita_pagamento(status) {
	if(status == 1) {
		$("#ModalParcela #data_pagamento").removeAttr("disabled").focus();
	}
	else {
		$("#ModalParcela #data_pagamento").attr("disabled","disabled").val('');
	}
}

function editar_parcela() {
	
	var base = $("#base").val();
	var parcela_id = $("#ModalParcela #parcela_id").val();
	var valor = $("#ModalParcela #valor").val();
	var data_vencimento = $("#ModalParcela #vencimento").val();
	var status = $("#ModalParcela #status").val();
	var data_pagamento = $("#ModalParcela #data_pagamento").val();
	
	$.ajax({
		url: base + 'clientes/editar_parcela?parcela_id=' + parcela_id + '&valor=' + valor + '&data_vencimento=' + data_vencimento + '&status=' + status + '&data_pagamento='+ data_pagamento,
		context: document.body
	}).done(function(retorno) {
		var r = JSON.parse(retorno);
		if(r.success) {
			$('#ModalParcela').modal('toggle');
			$("#alerta-parcela").removeClass('hidden').removeClass('alert-danger').addClass("alert-success").html(r.mensagem);
			lista_parcelas();
		}
		else {
			$("#alerta-parcela").removeClass('hidden').removeClass('alert-success').addClass("alert-danger").html(r.mensagem);
		}
	});
}

function excluir_parcela(parcela_id) {

	if(!confirm('Você tem certeza?'))
		return false;
	
	var base = $("#base").val();
	
	$.ajax({
		url: base + 'clientes/excluir_parcela?parcela_id=' + parcela_id,
		context: document.body
	}).done(function(retorno) {
		var r = JSON.parse(retorno);
		if(r.success) {
			$("#alerta-parcela").removeClass('hidden').removeClass('alert-danger').addClass("alert-success").html(r.mensagem);
			lista_parcelas();
		}
		else {
			$("#alerta-parcela").removeClass('hidden').removeClass('alert-success').addClass("alert-danger").html(r.mensagem);
		}
	});
	
}
